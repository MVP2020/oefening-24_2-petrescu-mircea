﻿using System;
using System.Windows;

namespace _24._2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }



        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Cirkel circle = new Cirkel(Convert.ToDouble(input.Text));
            Straalis.Content = "Straal: " + circle.Straal;
            Omtrek.Content = "Omtrek: " + circle.FormattedOmtrek().ToString();
            Oppervlakte.Content = "Oppervlakte: " + circle.FormattedOppervlakte().ToString();

        }
    }
}
