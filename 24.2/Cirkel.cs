﻿

using System;

namespace _24._2
{
    class Cirkel
    {
        public Cirkel(double straal)
        {
            Straal = straal;
        }
        private double _straal;
        public double Straal { get; set; }

        public double BerekenOmtrek()
        {
            return 2 * Math.PI * Straal;
        }
        public double BerekenOppervlakte()
        {
            return Math.PI * Straal;
        }
        public string FormattedOmtrek()
        {
            return Math.Round(BerekenOmtrek(), 2).ToString();
        }
        public string FormattedOppervlakte()
        {
            return Math.Round(BerekenOppervlakte(), 2).ToString();
        }

        //public string FormattedOmtrek()
        //{
        //    return
        //}


    }
}
